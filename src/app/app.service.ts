import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams }
    from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppService {
    headers: Headers;
    options: RequestOptions;

    constructor(private http: Http) {
        this.headers = new Headers();
        this.headers.append('X-Auth-Token', '46da28747a4c4de9a29b5ab957544156');
        this.headers.append('X-Response-Control', 'full');
        this.options = new RequestOptions({ headers: this.headers });
    }

    getService(url: string): Promise<any> {
        return this.http
            .get(url, this.options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getServiceOutro(url: string): Promise<any> {
        return this.http
            .get(url)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}