import { Component, OnInit, enableProdMode } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import * as moment from 'moment';
import 'rxjs/add/operator/map';
import { AppService } from './app.service';
import { promise } from 'protractor';


enableProdMode();


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService],
})


export class AppComponent {

  periodos = ['13-14', '14-15', '15-16', '16-17'];
  indices = [1, 2, 3, 4];
  objVisualizacao = [];
  loader = true;
  jogosPorPeriodo = [];


  constructor(private http: Http, private appService: AppService) {
    this.buscarDadosPorPeriodo().then((jogos) => {
      const retorno = this.obterEstatisticas(this.jogosPorPeriodo);
      if (retorno) {
        this.objVisualizacao.forEach(item => {
          item.dados.forEach(itemDados => {
            if (itemDados.indice === 1) {
              itemDados.timesUnder25 = itemDados.times.sort(this.compare25).slice(0, 5);
              itemDados.timesUnder35 = itemDados.times.sort(this.compare35).slice(0, 5);
            } else {
              itemDados.timesUnder25 = this.compararTimes(item.dados.find(a => a.indice === 1).timesUnder25, itemDados.times)
                .sort(this.compare25);
              itemDados.timesUnder35 = this.compararTimes(item.dados.find(a => a.indice === 1).timesUnder35, itemDados.times)
                .sort(this.compare35);
            }

          });
        });
      }
      this.loader = false;
    }).catch(error => {
      console.error(error);
    });
  }



  compararTimes(times1, times2) {
    const timesRetorno = [];
    times1.forEach(time => {
      timesRetorno.push(times2.find(a => a.time === time.time));
    });
    return timesRetorno;
  }

  obterEstatisticas(jogos) {
    jogos.forEach(itemPeriodo => {
      this.objVisualizacao.push({ periodo: itemPeriodo.periodo, dados: [] });
      this.indices.forEach(indice => {
        this.objVisualizacao.find(a => a.periodo === itemPeriodo.periodo).dados.push({ indice: indice, times: [] });
        const rodadas = this.rodadasPorIndice(indice, itemPeriodo.rodadas);
        itemPeriodo.times.forEach(time => {
          const jogosTime = this.buscarJogosTime(time.name, rodadas);
          const estatisticaTime = this.buscarEstatisticas(jogosTime, time.name);
          this.objVisualizacao.find(a => a.periodo === itemPeriodo.periodo)
            .dados.find(u => u.indice === indice).times.push(estatisticaTime);
        });
      });
    });
    return this.objVisualizacao;
  }
  buscarEstatisticas(jogosTime, time) {
    const retorno = {
      time: time,
      under05: (((jogosTime.filter(a => a.resultado < 1).length) * 100) / jogosTime.length),
      under15: (((jogosTime.filter(a => a.resultado < 2).length) * 100) / jogosTime.length),
      under25: (((jogosTime.filter(a => a.resultado < 3).length) * 100) / jogosTime.length),
      under35: (((jogosTime.filter(a => a.resultado < 4).length) * 100) / jogosTime.length),

      qtdUnder05: jogosTime.filter(a => a.resultado < 1).length,
      qtdUnder15: jogosTime.filter(a => a.resultado < 2).length,
      qtdUnder25: jogosTime.filter(a => a.resultado < 3).length,
      qtdUnder35: jogosTime.filter(a => a.resultado < 4).length,
    };
    return retorno;
  }

  buscarJogosTime(nomeTime, rodadas) {
    const jogosRodada = [];
    rodadas.forEach(rodada => {
      const jogo = rodada.jogos.find(a => a.away_team === nomeTime || a.home_team === nomeTime);
      if (jogo) {
        const resultadoSplit = jogo.match_result.split('-');
        // tslint:disable-next-line:radix
        jogo.resultado = parseInt(resultadoSplit[0]) + parseInt(resultadoSplit[1]);
        jogosRodada.push(jogo);
      }
    });
    return jogosRodada;
  }

  rodadasPorIndice(indice, rodadas) {
    if (indice === 1) {
      return rodadas.slice(0, 10);
    }
    if (indice === 2) {
      return rodadas.slice(10, 20);
    }
    if (indice === 3) {
      return rodadas.slice(0, 20);
    }
    if (indice === 4) {
      return rodadas.slice(20, 30);
    }
  }



  compare25(a, b) {
    if (a.qtdUnder25 > b.qtdUnder25) { return -1; }

    if (a.qtdUnder25 < b.qtdUnder25) { return 1; }

    return 0;
  }

  compare35(a, b) {
    if (a.qtdUnder35 > b.qtdUnder35) { return -1; }

    if (a.qtdUnder35 < b.qtdUnder35) { return 1; }

    return 0;
  }


  async buscarDadosPorPeriodo(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.jogosPorPeriodo = [];
      this.periodos.forEach(async (periodo, idx, arr) => {

        const p = { periodo: periodo, times: [], rodadas: [] };
        const arrayPromises = [this.buscarTimes(periodo), this.buscarRodadas(periodo)];

        await Promise.all(arrayPromises).then(async promises => {
          p.times = promises[0].data.teams;
          const rodadas = promises[1];

          const promisesRodadas = rodadas.data.rounds.map(rodada => {
            return this.buscarJogosRodada(rodada.round_slug, periodo);
          });

          await Promise.all(promisesRodadas).then((result: any) => {
            p.rodadas = result.map(x => {
              return { rodada: x.data.rounds[0].name, jogos: x.data.rounds[0].matches };
            });
            this.jogosPorPeriodo.push(p);
          }).catch(error => {
            reject({ error: 'Deu error porra!!' });
          });
        });

        if (idx === arr.length - 1) {
          resolve(this.jogosPorPeriodo);
        }
      });
    });

  }

  public buscarTimes(periodo: string): Promise<any> {
    return this.appService.getServiceOutro(`http://soccer.sportsopendata.net/v1/leagues/serie-a/seasons/${periodo}/teams`);
  }

  public buscarRodadas(periodo: string): Promise<any> {
    return this.appService.getServiceOutro(`http://soccer.sportsopendata.net/v1/leagues/serie-a/seasons/${periodo}/rounds`);
  }

  public buscarJogosRodada(rodada: string, periodo: string): Promise<any> {
    return this.appService
      .getServiceOutro(`http://soccer.sportsopendata.net/v1/leagues/serie-a/seasons/${periodo}/rounds/${rodada}`);
  }

}
