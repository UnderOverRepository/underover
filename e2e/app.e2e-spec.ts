import { MercadoOverPage } from './app.po';

describe('mercado-over App', () => {
  let page: MercadoOverPage;

  beforeEach(() => {
    page = new MercadoOverPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
